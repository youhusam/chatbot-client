import { LitElement, html, css } from 'lit-element';
import './chat-box';
import './text-input';

export class ChatBot extends LitElement {
  static get styles() {
    return css`
      * {
        font-family: sans-serif;
        box-sizing: border-box;
      }
      .main {
        padding: 20px 100px;
        height: 100%;
        display: flex;
        flex-direction: column;
        max-height: 100%;
      }
      chat-box {
        flex-basis: 100%;
        flex-shrink: 1;
        margin-bottom: 10px;
      }
    `;
  }
  constructor() {
    super();

    this.messages = [];
    this.userId = '';

    this.socket = io('http://localhost:4000/chat');
    this.socket.on('send:message', message => {
      this.messages = [...this.messages, message];
      this.requestUpdate();
    });
    this.socket.on('assign:id', reply => {
      this.userId = reply.id;
      this.requestUpdate();
    });
  }

  sendMessage(event) {
    if (event.detail.message !== '') {
      this.socket.emit('send:message', {
        message: event.detail.message,
        user: this.userId
      });
    }
  }


  render() {
    return html`
      <div class="main">
        <chat-box
          .messages="${this.messages}"
          .userId="${this.userId}"
        ></chat-box>
        <text-input @send-message="${this.sendMessage}"></text-input>
      </div>
    `;
  }
}

customElements.define('chat-bot', ChatBot);
