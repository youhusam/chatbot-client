import { LitElement, html, css } from 'lit-element';

export class ChatBox extends LitElement {
  static get properties() {
    return {
      messages: { type: Array },
      userId: { type: String }
    };
  }

  static get styles() {
    return css`
      :host {
        overflow: auto;
        background-color: #fff;
      }
      .container {
        display: flex;
        flex-direction: column;
        font-size: 14px;
        height: 100%;
        padding: 0 20px;
      }
      .bubble {
        border-radius: 50px 50px;
        padding: 10px 15px;
        margin: 20px 0 10px;
        margin-right: auto;
        background-color: #f1f1f1;
        max-width: 400px;
        position: relative;
      }
      .bubble .user-name {
        color: #aaa;
        position: absolute;
        right: auto;
        left: 15px;
        top: -20px;
        white-space: nowrap;
      }
      .bubble.current-user {
        margin-left: auto;
        margin-right: 0;
        background-color: #0099ff;
        color: #fff;
      }
      .bubble.current-user .user-name {
        left: auto;
        right: 15px;
      }
      .bubble.chatbot {
        margin-left: auto;
        margin-right: auto;
        text-align: center;
        background-color: #20cef5;
        color: #fff;
      }
      .bubble.chatbot .user-name {
        left: 0;
        right: 0;
      }
    `;
  }

  constructor() {
    super();

    this.messages = [];
    this.userId = '';
  }

  updated() {
    this.scrollTop = this.scrollHeight;
  }

  render() {
    return html`
      <div class="container">
        ${this.messages.map(m => {
          let className = 'bubble';
          let userName = 'Another User';
          switch (m.user) {
            case this.userId:
              className += ' current-user';
              userName = 'You';
              break;
            case 'chatbot':
              className += ' chatbot';
              userName = 'Chatbot';
              break;
            default:
              break;
          }

          return html`
            <div class="${className}">
              <div class="user-name">${userName}</div>
              ${m.message}
            </div>
          `;
        })}
      </div>
    `;
  }
}

customElements.define('chat-box', ChatBox);
