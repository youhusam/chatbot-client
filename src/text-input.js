import { LitElement, html, css } from 'lit-element';

export class TextInput extends LitElement {
  static get styles() {
    return css`
      :host {
        background-color: #fff;
      }
      .container {
        display: flex;
        flex-direction: row;
        font-size: 14px;
        padding: 0 20px;
        justify-content: space-between;
      }
      input {
        flex-basis: 100%;
        border-radius: 50px 50px;
        padding: 10px 15px;
        background-color: #f1f1f1;
        border: none;
        outline: none;
      }
      button {
        background: transparent;
        border: none;
        font-size: 50px;
        line-height: 0px;
        color: #0099ff;
        cursor: pointer;
      }
      button:hover {
        color: #66c2ff;
      }
      button:active {
        color: #006ab0;
      }
      button:disabled {
        color: #f1f1f1;
      }
    `;
  }

  constructor() {
    super();

    this.message = '';
  }

  changeMessage(e) {
    this.message = e.target.value;
    this.requestUpdate();
  }

  handleKeyPress(e) {
    if (e.target.value !== '') {
      if (e.key === 'Enter') {
        this.sendMessage({ message: this.message });
      }
    }
  }

  sendMessage() {
    let event = new CustomEvent('send-message', {
      detail: {
        message: this.message
      }
    });
    this.dispatchEvent(event);
    this.message = '';
    this.requestUpdate();
  }

  render() {
    return html`
      <div class="container">
        <input
          type="text"
          @input="${this.changeMessage}"
          @keypress=${this.handleKeyPress}
          .value="${this.message}"
          placeholder="Type your message..."
          autofocus
        />
        <button @click="${this.sendMessage}" .disabled="${this.message.length === 0}">➣</button>
      </div>
    `;
  }
}

customElements.define('text-input', TextInput);
