# ChatBot client

A chatbot client built with LitElement.

### Start a dev server

```
npm install -g polymer-cli
cd start-lit-element
npm install
polymer serve
```
